class Product < ActiveRecord::Base

	validates :price, numericality: true
	validates :quantity, numericality: { only_integer: true }

end
