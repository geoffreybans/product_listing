class ProductsController < ApplicationController

	def index
		@products = Product.all	
	end

	def create
		@product = Product.new(name: params[:name], quantity: params[:quantity], price: params[:price])
		
		if @product.save
			flash[:success] = "Product Added Successfully!"
			redirect_to @product			
		else
			@products = Product.all
			render 'index'
		end

	end

	def edit
		@product = Product.find(params[:id])
	end

	def show
		@products = Product.all	
	end

	def update
		@product = Product.find(params[:id])
		@product.name = params[:product][:name]
		@product.quantity = params[:product][:quantity]
		@product.price = params[:product][:price]
		
		if @product.save
			flash[:success] = "Product Update Success!"
			redirect_to @product
		else
			render 'edit'
		end
	end

	def destroy
		Product.find(params[:id]).destroy
		flash[:success] = "Product Delete Success!"

		redirect_to products_url
	end
end
