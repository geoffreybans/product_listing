This simple application enables you to add products to a database by filling out a form with the product details. 

### This application can: ###

* Add a new product
* List all products
* Edit the details of a product
* Delete a product from the database
* [View a live app at heroku](https://stormy-headland-71505.herokuapp.com/)

### How do I get set up? ###

These steps assume that you already have set up your Ruby on Rails development environment.

* Download the source code to your localhost
* Change directory to the root of your application in the `terminal`
* Run the `bundle install` to install the required gems
* Run `rails server` to start the WEBrick server
* Access the app through, for example `http://localhost:3000`

### How to use it ###

* Fill the product details in the form at the top of the table
* Product quantity cannot have decimal values, only whole numbers
* Price can have integer and decimal numbers mixed
* The default currency is $ USD
* NOTE: Only input the price without the $ sign in your new products form
* Click on the little pencil icon to edit the details of a product.
* The edit form is prefilled with the original product details, just edit as you please and save changes.
* Click in the `delete` link to remove a product from the database. You will need to confirm action before delete.